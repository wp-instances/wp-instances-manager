<?php

namespace WP_Instances\Manager;

class WP_Instance_Post_Type
{
    public const POST_TYPE = 'wp_instance';

    public function __construct()
    {
        add_action('init', [$this, 'register_post_type'], 10, 1);
        add_filter('manage_' . self::POST_TYPE . '_posts_columns', array($this, 'add_new_columns'));
        add_action('manage_' . self::POST_TYPE . '_posts_custom_column', array($this, 'populate_custom_columns'), 10, 2);
    }

    public function register_post_type()
    {
        register_post_type('wp_instance', [
            'labels' => [
                'name' => __('WP Instances', 'wp-instances'),
                'singular_name' => __('WP Instance', 'wp-instances'),
            ],
            'public' => false,
            'has_archive' => true,
            'menu_icon' => 'dashicons-admin-site',
            'supports' => ['title'],
            'show_in_rest' => true,
            'taxonomies' => array(),
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => false,
            'rewrite' => ['slug' => 'wp-instance'],
        ]);
    }

    public function add_new_columns($columns)
    {
        $columns['domain_name'] = __('Domain Name', 'wp-instances');
        $columns['core'] = __('Core', 'wp-instances');
        $columns['plugins'] = __('Plugins', 'wp-instances');
        $columns['themes'] = __('Themes', 'wp-instances');

        unset($columns['title'], $columns['date']);

        return $columns;
    }

    public function populate_custom_columns($column, $post_id)
    {
        $wp_instance_manager = new WP_Instance_Manager();
        $wp_instance = $wp_instance_manager->get_wp_instance($post_id);
        $wp_instance_array = $wp_instance->to_array();

        switch ($column) {
            case 'domain_name':
                $domain_name = $wp_instance_array['domain_name'];
                printf('<a href="%s" class="row-title">%s</a>', get_edit_post_link($post_id), $domain_name);
                break;
            case 'core':
                $core = $wp_instance_array['core'];
                
                $core_array = array();
                
                foreach ($core as $label => $value) {
                    $core_array[] = sprintf('%s: %s', $label, $value);
                }
                  
                echo implode(', ', $core_array);
                break;
            case 'plugins':
                $plugins = $wp_instance_array['plugins'];
                $plugins = array_map(function ($plugin) {
                    return $plugin['name'];
                }, $plugins);
                
                echo implode(', ', $plugins);
                break;
            case 'themes':
                $themes = $wp_instance_array['themes'];
                $themes = array_map(function ($theme) {
                    return $theme['name'];
                }, $themes);
                
                echo implode(', ', $themes);
                break;
        }
    }
}