<?php

namespace WP_Instances\Manager;

class WP_Instance_DTO {
    private string $domain_name;
    private array $core;
    private array $plugins;
    private array $themes;
    
    public function __construct(
        string $domain_name,
        array $core,
        array $plugins,
        array $themes
    ) {
        $this->domain_name = $domain_name;
        $this->core = $core;
        $this->plugins = $plugins;
        $this->themes = $themes;
    }
    
    public function get_domain_name(): string {
        return $this->domain_name;
    }
    
    public function get_core(): array {
        return $this->core;
    }
    
    public function get_plugins(): array {
        return $this->plugins;
    }
    
    public function get_themes(): array {
        return $this->themes;
    }
}