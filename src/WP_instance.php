<?php

namespace WP_Instances\Manager;

class WP_Instance
{
    private WP_Instance_DTO $wp_instance_dto;

    public function __construct(
        WP_Instance_DTO $wp_instance_dto
    ) {
        $this->wp_instance_dto = $wp_instance_dto;
    }

    public function get_wp_instance_dto(): WP_Instance_DTO
    {
        return $this->wp_instance_dto;
    }

    public function to_array(): array
    {
        $core = $this->wp_instance_dto->get_core();

        $core_array = array();

        foreach ($core as $key => $value) {
            $key = str_replace('_', ' ', $key);
            $key = ucwords($key);
            $core_array[$key] = $value;
        }

        return array(
            'domain_name' => $this->wp_instance_dto->get_domain_name(),
            'core' => $core_array,
            'plugins' => $this->wp_instance_dto->get_plugins(),
            'themes' => $this->wp_instance_dto->get_themes(),
        );
    }
}