<?php

namespace WP_Instances\Manager;

class WP_Instance_Manager
{

    public function insert_wp_instance(string $domain_name)
    {
        $wp_instance_data = $this->fetch_instance_data_from_api($domain_name);

        if (!$wp_instance_data) {
            return;
        }

        $core_data = $wp_instance_data['core'];
        $plugins_data = $wp_instance_data['plugins'];
        $themes_data = $wp_instance_data['themes'];

        // check if not exists
        $posts = get_posts(
            array(
                'post_type' => 'wp_instance',
                'meta_key' => 'domain_name',
                'meta_value' => $domain_name,
            )
        );

        if (count($posts) > 0) {
            return;
        }

        $post_args = array(
            'post_title' => $domain_name,
            'post_type' => 'wp_instance',
            'post_status' => 'publish',
            'meta_input' => array(
                'domain_name' => $domain_name,
                'core' => $core_data,
                'plugins' => $plugins_data,
                'themes' => $themes_data,
            ),
        );

        $post_id = wp_insert_post($post_args);

        return $post_id;
    }

    public function fetch_instance_data_from_api(string $domain_name): array
    {
        $endpoint_uri = 'https://' . $domain_name . '/wp-json/wp-instances-worker/v1/overview';

        $wp_instance_data = wp_remote_get(
            $endpoint_uri,
            array(
                'timeout' => 5,
                // 'sslverify' => false,
            )
        );

        $wp_instance_data = wp_remote_retrieve_body($wp_instance_data);
        $wp_instance_data = json_decode($wp_instance_data, true);

        return $wp_instance_data;
    }

    public function update_wp_instance(int $post_id)
    {
        $post = get_post($post_id);

        if (!$post) {
            return;
        }

        $domain_name = get_post_meta($post_id, 'domain_name', true);
        $wp_instance_data = $this->fetch_instance_data_from_api($domain_name);

        if (!$wp_instance_data) {
            return;
        }

        $core_data = $wp_instance_data['core'];
        $plugins_data = $wp_instance_data['plugins'];
        $themes_data = $wp_instance_data['themes'];

        update_post_meta($post_id, 'core', $core_data);
        update_post_meta($post_id, 'plugins', $plugins_data);
        update_post_meta($post_id, 'themes', $themes_data);
        
        return $post_id;
    }

    public function get_wp_instance(int $post_id): ?WP_Instance
    {
        $post = get_post($post_id);

        if (!$post) {
            return null;
        }

        $domain_name = get_post_meta($post_id, 'domain_name', true);
        $core = get_post_meta($post_id, 'core', true);
        $plugins = get_post_meta($post_id, 'plugins', true);
        $themes = get_post_meta($post_id, 'themes', true);

        $wp_instance_dto = new WP_Instance_DTO(
            domain_name: $domain_name,
            core: $core,
            plugins: $plugins,
            themes: $themes
        );

        $wp_instance = new WP_Instance($wp_instance_dto);

        return $wp_instance;
    }
}