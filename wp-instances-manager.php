<?php
/**
 * Plugin Name:     WP Instances Manager
 * Description:     Manage your WordPress instances from one place.
 * Author:          Filip Van Reeth
 * Author URI:      filipvanreeth.com
 * Text Domain:     wp-instances
 * Domain Path:     /languages
 * Version:         0.1.0
 * Network:         true
 * Requires PHP:    8.0
 */

use WP_Instances\Manager\WP_Instance_Post_Type;

add_action('init', function () {
    // var_dump('instances worker');
});

(new WP_Instance_Post_Type());

$wp_instance_manager = new WP_Instances\Manager\WP_Instance_Manager();
// $wp_instance_manager->insert_wp_instance('wordpress-native.lndo.site');

// $wp_instance = $wp_instance_manager->get_wp_instance(161);

function test_run()
{
    var_dump('test run');
    
}